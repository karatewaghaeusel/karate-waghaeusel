

![](../images/ts.jpg)

## Tahsin Sahin ##

- Präsident KLW
- Trainer
- 4\. Dan Karate

---

![](../images/je.jpg)

## Jan Emmerich ##

- Sportwart KLW
- Trainer
- 2\. Kyu Karate

---

![](../images/rw.jpg)

## Robert Wohlfeil ##

- Schatzmeister KLW
- 1\. Kyu Karate
