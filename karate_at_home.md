# Karate @ Home


## [Ten no Kata Übungen](https://www.youtube.com)
## [Kurzfassung](https://www.youtube.com)

---

## Ten no Kata Zusammenfassung
| Abkürzungen |                                             |      |                     |
| :-:         | :-:                                         | :-:  | :-:                 |
| ZK :        | Zenkutsu Dachi                              | FD : | Fudo Dachi          |
| KK :        | Kokutsu Dachi                               |      |                     |
| => :        | Schritt nach vorne                          | <= : | Schritt nach hinten |
| /  :        | hinteres Bein bleibt stehen Stand wechseln  |      |                     |

1.  ZK => Oi-Zuki Chudan
2.  ZK => Oi-Zuki Jodan
3.  FD => / ZK Gyaku-Zuki Chudan
4.  FD => / ZK Gyaku-Zuki Jodan
5.  FD <= Gedan-Barai / ZK Gyaku-Zuki
6.  FD <= Uchi Uke / ZK Gyaku-Zuki
7.  KK <= Shuto-Uke / ZK Nukite
8.  FD <= Shuto Uchi / ZK Gyaku-Zuki
9.  FD <= Age Uke / ZK Gyaku-Zuki
10. FD <= Tetsui Uchi / ZK Gyaku-Zuki

---

## Vorgeschlagener Übungsplan
| Tag     | Einheit 1 | Einheit 2 | Tag     | Einheit 1 | Einheit 2             |
| :-:     | :-----:   | :-:       | :-:     | :-:       | :-:                   |
| 1 - 3   | 1 + 2     | 1 + 2     | 18 - 21 | 6 + 7     | 7 + 8                 |
| 4 - 6   | 1 + 2     | 2 + 3     | 22 - 24 | 7 + 8     | 8 + 9                 |
| 7 - 9   | 2 + 3     | 3 + 4     | 25 - 27 | 8 + 9     | 9 + 10                |
| 10 - 12 | 3 + 4     | 4 + 5     | 28 - 30 | 9 + 10    | 10 + 11               |
| 13 - 15 | 4 + 5     | 5 + 6     | 31 - 33 | 10 + 11   | 11 + 12               |
| 16 - 18 | 5 + 6     | 6 + 7     | 33 - 35 | 11 + 12   | komplette Ten no Kata |
