# Ordnungen #

Hier findet ihr die Ordnungen für den KLW im PDF Format zum Download:

- [Dojo Etikette](downloads/TrainingsDojoEtikette.pdf)
- [Wettkampfordnung](downloads/WettkampfordnungKLW.pdf)
- [DKV Verfahrensordnung](downloads/DKVVerfahrensordnung.pdf)
- [Anhang Trainingsordnung](downloads/AnhangTrainingsordnung.pdf)
- [Satzung Oktober 2019](downloads/KLW_e_V_Satzung_Stand_Oktober_2019.pdf)
- [Beitragsordnung](downloads/Beitragsordnung_KLW.pdf)
- [Aufnahmeantrag und Datenschutzerklärung](downloads/Aufnahmeantrag_und_Datenschutzerklärung.pdf)

---

# Prüfungen #

- [Prüfungsprogramm Shotokan](downloads/shotokan.pdf)
- [Prüfungsprogramm SOK](downloads/sok.pdf)
- [Gürtelprüfungsordnung](downloads/GürtelprüfungsordnungKLW.pdf)
