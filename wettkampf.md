Unsere Karate Schule war schon auf Landes- und Bundesebene  
und vereinzelt auch international erfolgreich.  
Das Wettkampftraining findet momentan nicht statt.  
Bei Interesse wendet euch bitte an euren Trainer.  
Trainiert werden sowohl Kata als auch Kumite.  
