--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}

import Control.Applicative (empty)
import Data.Monoid
import Hakyll
import System.Directory
import Templates

--------------------------------------------------------------------------------
main :: IO ()
main = do
  imgio <- listDirectory "./images/"
  let images = ("./images/" ++) <$> imgio
  let titleField = constField "title" "Karate Leistungskader Waghäusel e.V."
  hakyll $ do
    match "images/*" $ do
      route idRoute
      compile copyFileCompiler

    match "downloads/*" $ do
      route idRoute
      compile copyFileCompiler

    match "main_image/*" $ do
      route idRoute
      compile copyFileCompiler

    match "css/*" $ do
      route idRoute
      compile compressCssCompiler

    match "js/*" $ do
      route idRoute
      compile copyFileCompiler

    match "fonts/*" $ do
      route idRoute
      compile copyFileCompiler

    match "*.md" $ do
      route $ setExtension "html"
      compile $ do
        item <- pandocCompiler
        templateItem <- makeItem (defTemplate defaultMarkdownBody)
        template <- compileTemplateItem templateItem
        finalItem <- applyTemplate template (bodyField "body" <> defaultContext) item
        relativizeUrls finalItem

    create ["index.html"] $ do
      route idRoute
      compile $ do
        templateItem <- makeItem (defTemplate indexBody)
        template <- compileTemplateItem templateItem
        makeItem ""
          >>= applyTemplate template titleField
          >>= relativizeUrls

    -- create ["bilder.html"] $ do
    --   route idRoute
    --   compile $ do
    --     let nums = [1 .. (length images - 1)]
    --     let numItems = Item "number" <$> nums :: [Item Int]
    --     makeItem ""
    --       >>= applyTemplate (defTemplate (imageBody images)) (titleField
    --                                                 <> listField "numbers" numCtx (return numItems)
    --                                                 <> defaultContext
    --                                                     )
    --       >>= relativizeUrls

    create ["anfahrt.html"] $ do
      route idRoute
      compile $ do
        templateItem <- makeItem (defTemplate anfahrtBody)
        template <- compileTemplateItem templateItem
        makeItem ""
          >>= applyTemplate template titleField
          >>= relativizeUrls

    create ["kontaktimpressum.html"] $ do
      route idRoute
      compile $ do
        templateItem <- makeItem (defTemplate contactBody)
        template <- compileTemplateItem templateItem
        makeItem ""
          >>= applyTemplate template titleField
          >>= relativizeUrls

-- match (fromList ["about.rst", "contact.markdown"]) $ do
--     route   $ setExtension "html"
--     compile $ pandocCompiler
--         >>= loadAndApplyTemplate "templates/default.html" defaultContext
--         >>= relativizeUrls

-- match "posts/*" $ do
--     route $ setExtension "html"
--     compile $ pandocCompiler
--         >>= loadAndApplyTemplate "templates/post.html"    postCtx
--         >>= loadAndApplyTemplate "templates/default.html" postCtx
--         >>= relativizeUrls

-- create ["archive.html"] $ do
--     route idRoute
--     compile $ do
--         posts <- recentFirst =<< loadAll "posts/*"
--         let archiveCtx =
--                 listField "posts" postCtx (return posts) <>
--                 constField "title" "Archives"            <>
--                 defaultContext
--         makeItem ""
--             >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
--             >>= loadAndApplyTemplate "templates/default.html" archiveCtx
--             >>= relativizeUrls

-- match "index.html" $ do
--     route idRoute
--     compile $ do
--         posts <- recentFirst =<< loadAll "posts/*"
--         let indexCtx =
--                 listField "posts" postCtx (return posts) <>
--                 constField "title" "Home"                <>
--                 defaultContext
--         getResourceBody
--             >>= applyAsTemplate indexCtx
--             >>= loadAndApplyTemplate "templates/default.html" indexCtx
--             >>= relativizeUrls

-- match "templates/*" $ compile templateBodyCompiler

--------------------------------------------------------------------------------
postCtx :: Context String
postCtx =
  dateField "date" "%B %e, %Y"
    `mappend` defaultContext

menuBodyCtx :: Context String
menuBodyCtx = bodyField "itemBody"

numCtx :: Context Int
numCtx = field "number" (return . show . itemBody)

imageUrlCtx :: Context String
imageUrlCtx = urlField "imgUrl"

-- singleField :: String -> Context a -> Compiler (Item a) -> Context b
-- singleField key c xs = singleFieldWith key c (const xs)

-- singleFieldWith :: String -> Context a -> (Item b -> Compiler (Item a)) -> Context b
-- singleFieldWith key c f = field' key $ fmap (ListField c) . f

field' :: String -> (Item a -> Compiler ContextField) -> Context a
field' key value = Context $ \k _ i -> if k == key then value i else empty
